﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DatabaseForm
{
/// <summary>
/// The main class called Form1
/// Contains all the methods for connecting to a database and displaying the form
/// </summary>
///<remarks>
///<para> This class can connect the user to a database using SQL connection</para>
///<para> This class also creates the login form</para>
///</remarks>

    public partial class Form1 : Form
    {
        //declaring a Sql connection    
        SqlConnection mySqlConnection;


        public Form1()
        {
            String[] myData = new String[100];
            InitializeComponent();

            //creating a sql connection
            mySqlConnection =
           new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Paddy\Documents\myDB.mdf;Integrated Security=True;Connect Timeout=30");

            //creating a string to select everything from the users table
            String selcmd = "SELECT * FROM Users ";

            //creating a new sql command
            SqlCommand mySqlCommand = new SqlCommand(selcmd, mySqlConnection);

            //opening the sql connection
            mySqlConnection.Open();

            //creating a sql data reader
            SqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

            int i = 0;
            while (mySqlDataReader.Read())
            {
                Console.WriteLine(mySqlDataReader["username"]); //reads a line of the query result at a time
                myData[i++] = (String)mySqlDataReader["username"]; //store in an array too for use later

            }

            for (int j = 0; j < i; j++) //now iterate through our array
            {
                Console.WriteLine("***" + myData[j] + "***");
            }
            


        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// The login button
        /// </summary>
        /// <remarks>
        /// Allows the user to login if they have a valid login
        /// </remarks>
        private void button1_Click(object sender, EventArgs e)
        {
            //creating the string that checks if the user has enetered valid login details
            String selcmd = "SELECT * FROM Users WHERE 'username'  = '" + textBox1.Text + "' AND 'password' '" + textBox2.Text + "' ";
            SqlCommand mySqlCommand = new SqlCommand(selcmd, mySqlConnection);

            this.Hide();  //if login details are correct, this form gets hidden

            var home = new Home();  //create a variable called home that is linked to the home page

            home.Show();  //display home
        }


        /// <summary>
        /// The register button
        /// </summary>
        /// <remarks>
        /// Allows the user to register if they do not have a login
        /// </remarks>
       

        private void button2_Click(object sender, EventArgs e)
        {


            this.Hide(); // if button is clicked the current form is hidden

            var register = new Register(); //create a new variable called register that is the register page

            register.Show(); //display the register page
        }
    }
}
