﻿using System.Windows.Forms;

namespace DatabaseForm
{

    /// <summary>
    /// An internal class called Home 
    /// Contains the home page
    /// </summary>
    ///<remarks>
    ///<para> This class naviagtes the user to their intended page</para>
    ///<para> This class creates 4 buttons that takes the user to all the possible pages they require</para>
    ///</remarks>
    
    internal class Home : Form
    {
        //declare the 4 buttons
        private Button button1;
        private Button button2;
        private Button button3;
        private Button button4;

        
        public Home()
        {
            InitializeComponent();
        }
      
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(99, 29);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(83, 34);
            this.button1.TabIndex = 0;
            this.button1.Text = "Report Bug";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(99, 80);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(83, 32);
            this.button2.TabIndex = 1;
            this.button2.Text = "Edit Bugs";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(99, 127);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(83, 35);
            this.button3.TabIndex = 2;
            this.button3.Text = "Open/Closed Bugs";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(99, 209);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "Exit";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Home
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Home";
            this.Text = "Homepage";
            this.Load += new System.EventHandler(this.Home_Load);
            this.ResumeLayout(false);

        }

        private void Home_Load(object sender, System.EventArgs e)
        {

        }
        /// <summary>
        /// The exit button
        /// </summary>
        /// <remarks>
        /// Allows the user to close the system
        /// </remarks>
        private void button4_Click(object sender, System.EventArgs e)
        {
            this.Close(); //when clicked the form will be closed 
        }

        /// <summary>
        /// The Add bug button
        /// </summary>
        /// <remarks>
        /// Allows the user to navigate to the add bug page
        /// </remarks>
       
        private void button1_Click(object sender, System.EventArgs e)
        {
            this.Hide(); //hides the current form
            var register = new Add(); //creates a new variable that is linked to the add bug form
            register.Show(); //displays the add bug form
        }


        /// <summary>
        /// The view bugs button
        /// </summary>
        /// <remarks>
        /// Allows the user to view the current bugs that need fixed
        /// </remarks>
   
        private void button2_Click(object sender, System.EventArgs e)
        {
            this.Hide(); //hides the current form
            var viewbug = new ViewBugs(); //creates a new variable that is linked to the view bugs form
            viewbug.Show(); //displays the view bugs form
        }


        /// <summary>
        /// The open/closed bugs button
        /// </summary>
        /// <remarks>
        /// Allows the user to view all the open and closed bugs 
        /// </remarks>
        
        private void button3_Click(object sender, System.EventArgs e)
        {
            this.Hide(); //hides the current form
            var openbug = new OpenBugs();//creates a new varibale that is linked to the open bugs form
            openbug.Show(); //displays the open bugs form
        }
    }
}