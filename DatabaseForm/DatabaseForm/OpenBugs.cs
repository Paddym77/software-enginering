﻿using System.Windows.Forms;

namespace DatabaseForm
{
    /// <summary>
    /// An internal class called OpenBugs
    /// Contains the Opens/Closed bugs page
    /// </summary>
    ///<remarks>
    ///<para> This class allows the user to view the bug history </para>
    ///<para> This class creates a buttons that takes the user back to the home form</para>
    ///</remarks>
    
    internal class OpenBugs: Form
    {
        //declare the back button
        private Button button1;

        public OpenBugs()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(103, 188);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Back";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // OpenBugs
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.button1);
            this.Name = "OpenBugs";
            this.Text = "Open/Closed Bugs";
            this.ResumeLayout(false);

        }

        /// <summary>
        /// The back button
        /// </summary>
        /// <remarks>
        /// Allows the user to go back to the previous page
        /// </remarks>
        
        private void button1_Click(object sender, System.EventArgs e)
        {
            this.Hide(); //hides the current form
            var home = new Home(); //creates a new variable that is linked to the home form
            home.Show(); //displays the home form
        }
    }
}