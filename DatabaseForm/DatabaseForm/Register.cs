﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DatabaseForm
{
    /// <summary>
    /// An internal class called Register that is linked to Form1
    /// Contains the register page
    /// </summary>
    ///<remarks>
    ///<para> This class adds the users inputted details into the user database</para>
    ///<para> This class also creates the registraton form</para>
    ///</remarks>
    
    internal class Register : Form
    {
        //declare all the tools used for this form
        private TextBox textBox1;
        private TextBox textBox2;
        private Label label1;
        private Label label2;
        private Button button2;
        private Button button1;


       
        public Register()
        {
            
            InitializeComponent();


           
        }

        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(94, 137);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Register";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(84, 45);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 1;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(84, 89);
            this.textBox2.Name = "textBox2";
            this.textBox2.PasswordChar = '*';
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "username:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "password:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(94, 166);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Sign In";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Register
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Name = "Register";
            this.Text = "Register";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        /// <summary>
        /// The register button
        /// </summary>
        /// <remarks>
        /// Allows the user to register their details and create a valid login
        /// </remarks>
        private void button1_Click(object sender, System.EventArgs e)
        {
            string username = textBox1.Text; //create the string for the username textbox
            string password = textBox2.Text; //create the string for the password textbox

            //create the query that will insert the inputted details into the user table
            string query = "INSERT INTO Users( username, password) " +
               "Values('" + username + "', '" + password + "')";

            //connect to the database via a sql connection 
            using (SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Paddy\Documents\myDB.mdf;Integrated Security=True;Connect Timeout=30")) 
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                }
            }

            this.Hide(); //hide the current form
            var home = new Home();//create a variable that is linked to the home page
            home.Show(); //display the home page
        }

        /// <summary>
        /// The login button
        /// </summary>
        /// <remarks>
        /// Allows the user to login if they have a valid login
        /// </remarks>
        private void button2_Click(object sender, System.EventArgs e)
        {
          
            this.Hide(); //hide the current form
            var home = new Form1(); //create a variable that is linked to form1
            home.Show(); //display form1
        }
    }
}