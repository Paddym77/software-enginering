﻿using System.Windows.Forms;

namespace DatabaseForm
{
    /// <summary>
    /// An internal class called ViewBugs
    /// Contains the ViewBugs page
    /// </summary>
    ///<remarks>
    ///<para> This class allows the user to view all the bugs that need fixed </para>
    ///<para> This class creates a button that allows the user to return to the previous page</para>
    ///</remarks>
    

    internal class ViewBugs: Form
    {
        //declare the back button
        private Button button1;

        public ViewBugs()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(102, 177);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Back";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ViewBugs
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.button1);
            this.Name = "ViewBugs";
            this.Text = "Bugs";
            this.ResumeLayout(false);

        }

        /// <summary>
        /// The back button
        /// </summary>
        /// <remarks>
        /// Allows the user to go back to the previous page
        /// </remarks>

        private void button1_Click(object sender, System.EventArgs e)
        {
            this.Hide(); //hides the current form
            var home = new Home();//creates a variable that is linked to the home form
            home.Show(); //display the home form
        }
    }
}